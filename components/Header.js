import React, {useState} from 'react';
import styles from "../styles/components/header.module.sass"
import Images from "../public/images";
import Image from "next/image";
import {RiSearchLine, RiArrowDownSLine} from "react-icons/ri";
import {BsBag} from "react-icons/bs";
import {GoThreeBars} from "react-icons/go";
import {TiTimes} from "react-icons/ti";


const Header = () => {
    const [showMenu, setShowMenu] = useState(false)

    return (
        <header className={styles.headerContainer}>
            <div className={styles.topSection}>
                <div className={styles.logoContainer}>
                    <span>
                        <Image src={Images?.zhaketLogo} alt={"logo"}/>
                    </span>
                    <div className={showMenu ? styles.main_container : ""} onClick={() => setShowMenu(false)}/>
                    <div className={styles.nav_icon} onClick={()=>setShowMenu(true)}>

                     <GoThreeBars/>

                    </div>


                </div>

                <div className={styles.userInfo}>
                    <div>
                        <RiSearchLine className={styles.icon}/>

                        <BsBag className={styles.icon}/>
                    </div>
                    <div>
                        <span>
                            <Image src={Images?.profileImg} alt={"ProfileImage"}/>
                        </span>

                        <RiArrowDownSLine className={styles.icon}/>
                    </div>

                </div>
            </div>
            <div className={styles.middleSection}>
                <ul>
                    <li>
                        محبوب ترین ها
                        <RiArrowDownSLine className={styles.icon}/>

                    </li>
                    <li>
                        فالب وردپرس
                        <RiArrowDownSLine className={styles.icon}/>

                    </li>
                    <li>
                        افزونه وردپرس
                        <RiArrowDownSLine className={styles.icon}/>

                    </li>
                    <li>
                        ژاکت سرویس
                        <RiArrowDownSLine className={styles.icon}/>

                    </li>
                    <li>
                        سایر
                        <RiArrowDownSLine className={styles.icon}/>

                    </li>
                    <li>
                        بلاگ
                        <RiArrowDownSLine className={styles.icon}/>

                    </li>
                    <li>
                        اموزش درامداینترنتی

                    </li>
                    <li>
                        ژاکت اکادمی
                        <RiArrowDownSLine className={styles.icon}/>

                    </li>
                </ul>

            </div>
            <div className={`${styles.responsiveMenuContainer} ${showMenu ? styles.showMenuContainer : ""}`}>
                <div className={`${styles.responsiveMenu} ${showMenu ? styles.showMenu : ""}`}>
                    <div className={styles.header}>
                        <div>

                            <span className={styles.sideBarText}>
                                Zhaket
                            </span>

                        </div>
                        <span onClick={() => setShowMenu(false)}><TiTimes /></span>
                    </div>
                    <div className={styles.sidebarItemsContainer}>
                        <div>
                            <span className={styles.sideBarText}>
                    محبوب ترین
                </span>
                        </div>
                        <div>
                            <span className={styles.sideBarText} >
                    قالب وردپرس
                </span>

                        </div>
                        <div>
                            <span className={styles.sideBarText}>
                    افزونه وردپرس
                </span>
                        </div>
                        <div>
                            <span className={styles.sideBarText}>
                   ژاکت سرویس
                </span>
                        </div>
                        <div>
                            <span className={styles.sideBarText}>
                    سایر
                </span>
                        </div>
                        <div>
                            <span className={styles.sideBarText}>
                    بلاگ
                </span>
                        </div>
                        <div>
                            <span className={styles.sideBarText}>
                    اموزش درامداینترنتی
                </span>
                        </div>

                        <div>
                            <span className={styles.sideBarText}>
                    ژاکت اکادمی
                </span>
                        </div>

                    </div>

                </div>
            </div>

        </header>
    );
};

export default Header;