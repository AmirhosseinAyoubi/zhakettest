import React from 'react';
import styles from "../styles/components/product.module.sass"
import Image from "next/image";
import Images from "../public/images";
import ReactStars from "react-rating-stars-component";
import {e2p} from "../utiliy/globalFunctions";

// products card component

const Product = ({
                     title,
                     short_description,
                     price,
                     total_sales,
                     coverImage,
                     storeName,
                     storeDesc,
                     storeLogo,
                     has_auto_update
                 }) => {
    return (
        <div className={styles.cardContainer}>
            <div className={styles.cardImage}>
                <span className={styles.cardCover}>
                      <img src={coverImage} alt={"product"}/>
                </span>

                <div className={styles.productInfo}>
                    <div>
                        <span>
                            <Image src={storeLogo} layout={"fill"}/>
                        </span>
                        <div>
                            <h4>{storeName}</h4>
                            <p>{storeDesc}</p>
                        </div>
                    </div>

                </div>
            </div>
            <div>
                <p>{title}</p>
                <div>
                    <span>
                        {short_description}
                    </span>
                    <div className={styles.rate}>
                        <div>
                            <div>
                        <span>
                            {e2p(total_sales)}
                        </span>
                                فروش
                            </div>
                            <div>
                        <span>
                           {e2p(4.50)}
                        </span>
                                <ReactStars
                                    count={1}
                                    size={18}
                                    color="#fea000"

                                    edit={false}
                                />
                            </div>
                        </div>
                        <div>
                            <span>{e2p(price)}</span>هزارتومان
                        </div>
                    </div>
                </div>
            </div>
            {has_auto_update && <div className={styles.autoUpdate}>
                <div>
                    بروزرسانی خودکار
                </div>
            </div>}
        </div>);
};

export default Product;