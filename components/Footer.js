import React from 'react';
import styles from "../styles/components/footer.module.sass"
import Link from "next/link";
import Images from "../public/images";
import Image from "next/image";
import {FaTelegramPlane} from "react-icons/fa";
import {FiInstagram} from "react-icons/fi";
import {RiLinkedinFill} from "react-icons/ri";
import {CgFacebook} from "react-icons/cg";
import {AiOutlineTwitter} from "react-icons/ai";

const Footer = () => {
    return (
        <footer className={styles.footerContainer}>
            <div className={styles.topMenu}>
                <ul>
                    <li>
                        <Link href={"/"} passHref>
                            قوانین ژاکت
                        </Link>
                    </li>
                    <li>
                        <Link href={"/"} passHref>
                            مراحل خرید قالب وردپرس
                        </Link>
                    </li>
                    <li>
                        <Link href={"/"} passHref>
                            لوگو
                        </Link>
                    </li>
                </ul>
                <div className={styles.footerLogo}>
                    <Image src={Images?.footerLogo} alt={"ZhakerLogo"}/>
                </div>
                    <ul>
                        <li>
                            <Link href={"/"} passHref>
                                فروشنده شوید
                            </Link>
                        </li>
                        <li>
                            <Link href={"/"} passHref>
                                مشتریان وفادار
                            </Link>
                        </li>
                        <li>
                            <Link href={"/"} passHref>
                                درباره ما
                            </Link>
                        </li>
                        <li>
                            <Link href={"/"} passHref>
                                تماس باما
                            </Link>
                        </li>
                    </ul>

            </div>
            <div className={styles.bottomMenu}>
                <div className={styles.socialMedia}>
                    <span>ژاکت در شبکه های اجتماعی</span>
                    <span>
                        <FaTelegramPlane className={styles.icon}/>
                        <FiInstagram className={styles.icon}/>
                        <RiLinkedinFill className={styles.icon}/>
                        <CgFacebook className={styles.icon}/>
                        <AiOutlineTwitter className={styles.icon}/>
                    </span>
                </div>
                <div>
                    <Image src={Images?.eNamad} alt={"E_Namad"}/>
                </div>
            </div>
        </footer>
    );
};

export default Footer;