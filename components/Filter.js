import React from 'react';
import styles from "../styles/components/filter.module.sass"
import {RiSearchLine} from "react-icons/ri";


// filter products component

const Filter = () => {


    return (
        <div className={styles.filtersContainer}>

            <div className={styles.lastSection}>
                <div>

                    نتایج جستوجو

                </div>

            </div>

            <div className={styles.filtersOptions}>
                <div className={styles.searchInput}>
                    <input type={"text"} placeholder={"برای مثال قالب فروشگاهی"}/>
                    <RiSearchLine className={styles.icon}/>
                </div>

                <div className={styles.roles}>
                    <div>
                        <span className={styles?.rolesTitle}>مرتب سازی براساس:</span>
                        <div>
                            <input  type="radio" name="role" value="ONE" id="one"/>
                            <label htmlFor="#one">پرفروش ترین</label>
                        </div>
                        <div>
                            <input  type="radio" name="role" value="ONE" id="two"/>
                            <label htmlFor="#two">پرتخفیف ها</label>
                        </div>
                        <div>
                            <input  type="radio" name="role" value="ONE" id="three"/>
                            <label htmlFor="#three">جدیدترین</label>
                        </div>
                        <div>
                            <input  type="radio" name="role" value="ONE" id="four"/>
                            <label htmlFor="#four">آخرین بروزرسانی ها</label>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    );
};

export default Filter;