import React, {useState} from 'react';
import styles from "../styles/components/discount.module.sass"

const Discount = () => {
    const [hide,setHide]=useState(false)
    return (
            <div className={`${styles.discountContainer} ${hide?styles.hide:null}`}>
            <div>
                <div onClick={()=>setHide(!hide)}>
                    X
                </div>
                <p>تمدیدجشنواره ژاکت تا عید غدیربا تخفیف های ویژه از ۳۰٪ تا ۷۰٪ +جوایز جذاب</p>
            </div>
            <div>
                <button>
                    بریم جشنواره قربان تا غدیر ژاکت
                </button>
            </div>

        </div>
    );
};

export default Discount;