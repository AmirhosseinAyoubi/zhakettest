import axiosInstance from "../../utiliy/axios";

export const getProducts = async () => {
    return ( axiosInstance.post('public-product/latest-products',null)
        .then((resp) => resp)
        .catch((error) => error.response))
}
