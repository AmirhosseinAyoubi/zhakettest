import {call, put, takeLatest} from "redux-saga/effects";
import {getProducts} from "./request";
import {success, Error, actionTypes} from "../actions";

 function* handleGetProducts() {
    try {
        const response = yield call(getProducts)
        const {data} = response
        yield put(success(data.payload))
    } catch (error) {
        yield put(Error(error))
    }
}
 function* watchDataLoad() {
    yield takeLatest(actionTypes.GET_PRODUCTS_REQUEST, handleGetProducts)
}
export default  watchDataLoad