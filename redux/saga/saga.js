import { all} from 'redux-saga/effects'
import handler, {handleGetProducts} from "./handler";




export default function* rootSaga() {
    yield all([
        handler()
    ]);
}