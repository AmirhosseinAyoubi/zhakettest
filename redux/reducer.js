import { actionTypes } from './actions'
import { HYDRATE } from 'next-redux-wrapper'

const initialState = {
    loading: false,
    products:null,
    error: "",

}

function reducer(state = initialState, action) {
    switch (action.type) {
        case HYDRATE: {
            return { ...state, ...action?.payload }
        }

        case actionTypes.GET_PRODUCTS_REQUEST:
            return {
                ...state,
                loading: true
            }

        case actionTypes.GET_PRODUCTS_SUCCESS:
            // console.log(action.payload)
            return {
                loading: false,
                products: action?.payload,
                error: ""
            }

        case actionTypes.GET_PRODUCTS_FAIL:
            return {
                ...state,
                loading: false,
                error: action?.payload
            }


        default:

            return state
    }
}

export default reducer