export const actionTypes = {
    GET_PRODUCTS_REQUEST: "GET_PRODUCTS_REQUEST",
    GET_PRODUCTS_SUCCESS: "GET_PRODUCTS_SUCCESS",
    GET_PRODUCTS_FAIL: "GET_PRODUCTS_FAIL",
}

export function pending() {
    return {
        type: actionTypes.GET_PRODUCTS_REQUEST
    }
}

export function success(data) {
    return {type: actionTypes.GET_PRODUCTS_SUCCESS,payload:data}
}

export function Error(err) {
    return {type: actionTypes.GET_PRODUCTS_FAIL,payload:err}
}

