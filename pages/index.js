import Discount from "../components/Discount";
import Layout from "../components/Layout";
import styles from "../styles/pages/index.module.sass"
import Filter from "../components/Filter";
import Product from "../components/Product";
import {wrapper} from "../redux/store";
import {pending} from "../redux/actions";


function Home({loading,products}) {


    return (
        <>
            <Discount/>
            <Layout>
                <Filter/>


                <div className={styles.productsContainer}>
                    {
                        products?.map(item => {
                                return (
                                    <Product key={item?.id}
                                             title={item?.title}
                                             short_description={item?.short_description}
                                             price={item?.price}
                                             total_sales={item?.total_sales}
                                             coverImage={`https://cdn.zhaket.com/resources/${item?.cover?.path}/${item?.cover?.id}.${item?.cover?.name?.split(".").slice(-1)}`}
                                             storeDesc={item?.stores?.name}
                                             storeLogo={`https://cdn.zhaket.com/resources/${item?.stores?.logo?.path}/${item?.stores?.logo?.id}.${item?.stores?.logo?.name?.split(".").slice(-1)}`}
                                             storeName={item?.stores?.slogan}
                                             has_auto_update={item?.has_auto_update}
                                    />
                                )
                            }
                        )
                    }


                </div>


            </Layout>
        </>
    )
}

export const getServerSideProps = wrapper.getServerSideProps(store => async () => {
    if (!store.getState()?.products?.length > 0) {
        store.dispatch(pending())

    }

    await new Promise(res => setTimeout(res, 1000));
    return {props: store.getState()};
});

export default Home;