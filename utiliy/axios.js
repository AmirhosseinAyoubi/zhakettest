import axios from 'axios'

// axios instance
const axiosInstance = axios.create({
    baseURL: 'https://api.zhaket.com/',
    timeout: 10000,

})

export default axiosInstance

