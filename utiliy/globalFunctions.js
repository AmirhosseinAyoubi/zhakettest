//global functions




                                        ////////
export function e2p(input){             // convert english digits to persian
                                        ////////
    if (input === undefined)
        return;
    let returnModel = "", symbolMap = {
        '1': '۱',
        '2': '۲',
        '3': '۳',
        '4': '۴',
        '5': '۵',
        '6': '۶',
        '7': '۷',
        '8': '۸',
        '9': '۹',
        '0': '۰'
    };
    input = input.toString();
    for (let i = 0; i < input.length; i++)
        if (symbolMap[input[i]])
            returnModel += symbolMap[input[i]];
        else
            returnModel += input[i];
    return returnModel;
}